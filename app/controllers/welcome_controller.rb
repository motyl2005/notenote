class WelcomeController < ApplicationController
  def index
    @notes = Note.all.order("created_at DESC")
  end
end
