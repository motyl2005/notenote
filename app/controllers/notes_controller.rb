class NotesController < ApplicationController
  before_action :find_note, only: [:show, :edit, :update, :destroy ]
  before_action :authenticate_user!, only: [:new, :create, :edit, :update, :destroy]
  
  def index
    @notes = Note.where(user_id: current_user).order("created_at DESC")
  end
  
  def show
  end
  
  def new
    @note = current_user.notes.build
    @note.user_id = current_user.id
  end
  
  def create
    @user = current_user
    @note = @user.notes.build(note_params)
    @note.user_id = current_user.id
    if @note.save
      redirect_to @note
    else
      render 'new'
    end
  end
  
  def edit
  end
  
  def update
    if @note.update(note_params)
      redirect_to @note
    else
      render 'edit'
    end
  end
  
  def destroy
    if @note.destroy
      redirect_to notes_path
    else
      redirect_to @note
    end
  end


private

def find_note
  @note = Note.find(params[:id])
end

def note_params
  params.require(:note).permit(:title, :content, :user_id)
end

end
